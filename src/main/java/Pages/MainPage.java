package Pages;

import com.codeborne.selenide.SelenideElement;
import core.WebExtensions;

import static com.codeborne.selenide.Selenide.$x;

public class MainPage extends WebExtensions{

	private SelenideElement firstLoginButton = $x("//ul[@id='regmenu']/li[1]/a");
	private SelenideElement secondLoginButton = $x("//ul[@id='regmenu']//button[@type='submit']");
	private SelenideElement emailField = $x("//li//input[@name='login']");
	private SelenideElement passwordField = $x("//li//input[@name='password']");


	public void clickOnFirstLoginButton(){
		log.info("Click on first login button");
		firstLoginButton.click();
	}

	public void typeEmail(String email){
		log.info("Type email: " + email + " in email field");
		emailField.setValue(email);
	}

	public void typePassword(String password){
		log.info("Type password: " + password + " in password field");
		passwordField.setValue(password);
	}

	public void clickOnSecondLoginButton(){
		log.info("Click on second login button");
		secondLoginButton.click();
	}

}
