package Pages;

import com.codeborne.selenide.SelenideElement;
import core.WebExtensions;

import static com.codeborne.selenide.Selenide.$x;

public class AdminPanelPage extends WebExtensions{

	private SelenideElement messengersSection = $x("//a[@href='/messengers/']");


	public void clickOnMessengersSection(){
		log.info("Click on 'Messengers' section");
		messengersSection.click();
	}
}
