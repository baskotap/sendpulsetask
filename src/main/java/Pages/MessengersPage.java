package Pages;

import com.codeborne.selenide.SelenideElement;
import core.WebExtensions;
import org.testng.Assert;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.refresh;

public class MessengersPage extends WebExtensions{

	private SelenideElement signUpWidgetsButton = $x("//a[@href='/messengers/widgets/']");
	private SelenideElement addWidgetButton = $x("//a[@href='/messengers/widgets/new/']");
	private SelenideElement createdWidgetColumn = $x("//div[@class='table-responsive']//tr[@class='ng-scope'][1]");
	private SelenideElement widgetNameLabel = $x("//h4[@class='media-heading']/a");
	private SelenideElement actionsButton = $x("//tr[2]//button[@data-toggle='dropdown'][1]");
	private SelenideElement editActionButton = $x("//ul[@class='dropdown-menu dropdown-menu-right']//li[1]/a");
	private SelenideElement deleteActionButton = $x("//ul[@class='dropdown-menu dropdown-menu-right']//li[5]/a");
	private SelenideElement modalDeleteButton = $x("//button[@ng-disabled='deleting']");
	private SelenideElement modalCancelButton = $x("//button[@ng-click='cancel()']");
	private SelenideElement widgetPicture = $x("//img[@class='panel-logo']");


	public void clickOnSignupWidgets(){
		log.info("Click on 'Signup widgets' section");
		signUpWidgetsButton.click();
	}

	public void clickOnAddWidgetButton(){
		log.info("Click on 'Add widget' section");
		addWidgetButton.click();
	}

	public void checkCreatedWidget(){
		log.info("Scroll to element");
		createdWidgetColumn.scrollTo();
		log.info("Check widget after created");
		Assert.assertTrue(createdWidgetColumn.isDisplayed());
	}

	public void checkWidgetAfterDeleted(){
		log.info("Refresh page");
		refresh();
		log.info("Check widget after deleted");
		Assert.assertFalse(createdWidgetColumn.isDisplayed());
	}

	public void checkWidgetName(String expectedWidgetName){
		Assert.assertEquals(expectedWidgetName, widgetNameLabel.getText());
	}

	public void clickOnActionsButton(){
		log.info("Click on actions");
		actionsButton.click();
	}

	public void clickOnEditButton(){
		log.info("Click on 'Edit' button");
		editActionButton.click();
	}

	public void clickOnDeleteWidgetButton(){
		log.info("Click on 'Delete' action button");
		deleteActionButton.click();
	}

	public void clickOnModalDelete(){
		log.info("Click on 'Delete' button");
		modalDeleteButton.click();
	}

}
