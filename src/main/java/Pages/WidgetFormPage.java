package Pages;

import com.codeborne.selenide.SelenideElement;
import core.WebExtensions;

import static com.codeborne.selenide.Selenide.$x;

public class WidgetFormPage extends WebExtensions{

	private SelenideElement formTypeBar = $x("//img[@ng-src='/img/messengers/widgets/001.svg']/following-sibling::span");
	private SelenideElement nextButton = $x("//button[contains(text(),'Next')]");
	private SelenideElement saveButton = $x("//button[contains(text(),'Save and get code')]");
	private SelenideElement modalDoneButton = $x("//div[@class='modal-footer']//a[2]");
	private SelenideElement modalCancelButton = $x("//div[@class='modal-footer']//a[1]");
	private SelenideElement widgetNameField = $x("//label[contains(text(),'Widget name')]/following-sibling::input");


	public void selectBarFormType(){
		log.info("Select 'Bar' form type");
		formTypeBar.click();
	}

	public void clickOnNextButton(){
		log.info("Click on 'Next' button");
		nextButton.click();
	}

	public void clickOnSaveWidgetButton(){
		log.info("Click on 'Save and' get code button");
		saveButton.click();
	}

	public void clickOnDone(){
		log.info("Click on 'Done' in ");
		modalDoneButton.click();
	}

	public void typeWidgetName(String widgetName){
		log.info("Type: " + widgetName + " in widget name field");
		widgetNameField.setValue(widgetName);
	}


	public String getWidgetName(){
		return widgetNameField.getAttribute("value");
	}
}
