import Pages.AdminPanelPage;
import Pages.MainPage;
import Pages.MessengersPage;
import Pages.WidgetFormPage;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import static com.codeborne.selenide.Selenide.open;

public class MainTest{

	protected MainPage       mainPage       = new MainPage();
	protected AdminPanelPage adminPanelPage = new AdminPanelPage();
	protected MessengersPage messengersPage = new MessengersPage();
	protected WidgetFormPage widgetFormPage = new WidgetFormPage();

	@BeforeSuite
	void setUp(){
		open("https://sendpulse.com/");
	}


	@AfterSuite
	void finish(){
		System.out.println("Tests were finished");
	}
}
