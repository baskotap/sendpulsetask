import org.testng.annotations.Test;

public class WidgetsTest extends MainTest{

	@Test(priority = 1)
	void createNewWidget(){
		mainPage.clickOnFirstLoginButton();
		mainPage.typeEmail("baskosergey93@gmail.com");
		mainPage.typePassword("11111");
		mainPage.clickOnSecondLoginButton();
		adminPanelPage.clickOnMessengersSection();
		messengersPage.clickOnSignupWidgets();
		messengersPage.clickOnAddWidgetButton();
		widgetFormPage.selectBarFormType();
		widgetFormPage.clickOnNextButton();
		widgetFormPage.typeWidgetName("Test widget");
		widgetFormPage.clickOnSaveWidgetButton();
		widgetFormPage.clickOnDone();

		messengersPage.checkCreatedWidget();
	}

	@Test(priority = 2)
	void editWidgetName(){
		messengersPage.clickOnActionsButton();
		messengersPage.clickOnEditButton();
		widgetFormPage.typeWidgetName("Edit Widget Name");
		String name  = widgetFormPage.getWidgetName();
		widgetFormPage.clickOnSaveWidgetButton();
		widgetFormPage.clickOnDone();

		messengersPage.checkWidgetName(name);
	}

	@Test(priority = 3)
	void deleteWidget(){
		messengersPage.clickOnActionsButton();
		messengersPage.clickOnDeleteWidgetButton();
		messengersPage.clickOnModalDelete();

		messengersPage.checkWidgetAfterDeleted();
	}

}